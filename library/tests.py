from django.test import TestCase
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class library_unit_test(TestCase):
    def test_status_url(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_redirect(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_url_json_data(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code, 200)

class library_functional_test(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(library_functional_test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(library_functional_test, self).tearDown()

    def test_search(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        selenium.implicitly_wait(10)
        time.sleep(5)

        searchinput = selenium.find_element_by_id('book-search')
        button = selenium.find_element_by_id('b1')
        selenium.implicitly_wait(10)
        time.sleep(5)

        searchinput.send_keys('Love')
        selenium.implicitly_wait(5)
        time.sleep(5)

        button.send_keys(Keys.RETURN)

